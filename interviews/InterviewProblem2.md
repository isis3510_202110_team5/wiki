INTERVIEW STRUCTURE

1.	Hello! How are you? What is your name?
Hello, I’m fine thank you. My name is Valentina Zapata Coronel.
2.	What kind of pet do you have?
I am the mother of a beautiful dog
3.	What is the name of your pet?
His name is Theo
4.	What do you do?
I’m a fulltime college student .
5.	Do you live in a house, or a flat?
I live in a House
6.	Can you talk to me about your pet?
Yes. Theo is a French PooddleMminy Toy. He weights around 7 kg, so you could say he is a little overweight. He is a mama’s boy. He likes to be always with you. At all times, when you sleep, when you go the bathroom, when you eat… He needs to be close.
7.	Who do you live with?
I live with my family; my mother, my brother and me. We all love our doggie.
8.	How is your relationship with your pet?
I am really close to him. I love him above everything and treat him as my own child. He is my sun.
9.	When you go out with who do you leave your pet?
I usually leave him with a close family member (Mother or Brother), but if we all go out, he stays alone.
10.	How often do you leave your pet alone?
Not that often.
11.	How much was longest time you left him alone?
When we go on family vacation, we usually don’t take him, so about two weeks.
12.	How do you feel about it?
I hate leaving him behind, because he is a part of our family, and he depends on us.
13.	How do you think your pet feels about it?
He does not like him. He hates being alone. I guess he just gets too bored.
14.	Have you ever tried pethotels?
Yes, I’ve tried them before. I don’t like them because they put your dog in a cage with minimum space for days. My baby needs company and love.
15.	Have you ever had a bad experience with a pethotel?
Besides all of what I just said, the logistics aren’t the best. Most places, you must call first, and often they just don’t answer. Then you must take them to the place, and they are sometimes full. You end up super stressed because you dot know what to do next.
16.	Have you ever left you dog with your neighbors?
I believe I am lucky because my neighbors love Theo as much as I do. The are willing to dogsit him for a few days. However, in vacation time, they are often gone. 
17.	Would you leave your pet with a stranger? 
I would consider it only if I can state how much the stranger cares for dogs. If I see he or she loves them as much as I do, I would leave Theo with them.
18.	Would you willing if he has pets?
Yes, that could be a good sign that he or she has tenderness and love for doggies.
19.	And finally, can you give me a photo of Theo. 
Yes of course, here.

