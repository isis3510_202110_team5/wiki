# Interview Structure

### Problem
Users don't know where to go when they decide to eat at a restaurant. Some recommendations help to get an idea about the place but there is no information about the plates that are served. What if there is something very bad at the menu of a good restaurant ? Is it possible that you are unlucky enough that you get to choose that plate ?

### Solution
App to organize and rate users favorite restaurants and dishes from those restaurants.

The defined interview structure pretends to empathize, understand the context and the user of a restaurant.

### Script

*How frequently do you eat at home?*

I eat at home almost every day my 3 meals a day

*How frequently do you eat in a restaurant, in place or delivery ?*

I eat in a restaurant 2 times a week, mostly weekends

*Which is your favorite restaurant?*

Crepes and Waffles

*What is your favorite dish ?*

Chicken crepe

*What is your favorite type of food, Mexican, American, Italian, etc ?*

American food

*Have you ever given your opinion of a restaurant on Google maps or Tripadvisor or any other app ?*

Yes, at tripadvisor

*Do you think it is useful to read the recommendations of these restaurants ? Why ?*

Yes, to have an idea of the place even though sometimes I don’t follow their recommendations.

*Do you consider it would be useful to also have recommendations of dishes ? Why ?*

Yes, a lot. Because I could have an idea of the best dishes of a place instead of improvising.

*Would you like to have an app where you could organize and rate your favorite restaurants and dishes ?*

Yes, with points or rankings, and that way you have a chance to win a price in exchange of the opinion.
