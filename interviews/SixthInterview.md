# Interview Structure

### Context
How is the process that you as a martial arts practitioner have done in COVID-19 pandemic, how has the transition from face-to-face classes to virtual classes been, if there are some things that are not the same as face-to-face, and difficulties.

### Content

1- Felipe's introduction.

2- Kendo transition from presence to virtuality.

3- How Kendo Uniandes began to adapt to virtuality.

4- Theoretical classes rather than practical ones.

5- On choosing streaming platforms according to need.

6- Physical classes on these platforms in the Kendo selection.

7- Physical classes for Kendo beginners.

8- Loss of community.

10- Communities on facebook and whatsapp.

11- Communities between martial arts.

12-Cyberspaces.

13-Beginner Videos / Exercise Plan.
