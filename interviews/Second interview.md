Problem: It is difficult for patients to measure their pain index and express it to their doctors when they are in an orthopedic rehabilitation process.
This interview has the objective of empathizing and understanding the problems when performing an orthopedic rehabilitation.

The following questions were asked:
Hello, what is your name?
How old are you?
What do you work on?
Have you ever had an accident that caused you to need orthopedic rehabilitation?
How long were you in recovery?
Did you have trouble remembering what times to take medications or go to medical appointments?
How often did you have a control appointment?
In your medical check-ups, did they ask you about how much pain you had had in the previous days?
Did you have a way to measure the rate of pain that you felt every day and at your medical check-ups tell your doctor about it?
How satisfied were you with your recovery?
How satisfied do you feel with your relationship with your doctor during the rehabilitation process?

The interview was carried out through recordings that can be seen below.

